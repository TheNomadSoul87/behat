<?php

require __DIR__.'/vendor/autoload.php';

use Behat\Mink\Driver\GoutteDriver;
use Behat\Mink\Session;
use Behat\Mink\Driver\Selenium2Driver;


// Important object #1
$driver = new GoutteDriver();

// Important object #2
//Considérez la session comme un onglet de navigateur : tout ce que vous pouvez faire dans un onglet, vous pouvez le faire dans une session. Et en fait, ce n'est pas beaucoup. Vous pouvez visiter les URL, actualiser, revenir en arrière, avancer et c'est à peu près tout. Utilisons-le pour visiter un site très impressionnant et absurdement conçu "jurassicpark.wikia.com".
$session = new Session($driver);

// Pour exécuter le code => terminal : php mink.php
$session->start();
$session->visit('http://jurassicpark.wikia.com');

echo "Status code: ". $session->getStatusCode() . "\n";
echo "Current URL: ". $session->getCurrentUrl() . "\n";

// Important object #3 DocumentElement
// Je veux que vous considériez cet objet comme l'objet JQuery ou le DOM.
// Tout ce que vous pouvez faire avec JQuery - comme sélectionner des éléments, cliquer sur des liens et remplir des champs - vous pouvez le faire avec la page. Moins impressionnant, il connaît également le code HTML de la page sur laquelle nous nous trouvons actuellement.
// Si vous aimez creuser dans le code source, la page est une instance de DocumentElement.
$page = $session->getPage();

// La méthode getText() renvoie autre chose que les balises HTML elles-mêmes. La première partie provient de la balise title, puis elle récupère d'autres éléments de la balise style, qui sont techniquement du texte.
echo "First 75 chars: ".substr($page->getText() , 0, 75) . "\n";

// Important object #4 NodeElement
// Dès que vous trouvez un seul élément, vous avez maintenant un fichier NodeElement. Ce qui est cool, c'est que ce nouvel objet a toutes les mêmes méthodes que la page, plus un tas d'extras qui s'appliquent aux éléments individuels.
//$header = $page->find('css', '.wds-community-header__sitename a');
//
//echo "The wiki site name is: ".$header->getText()."\n";

$subNav = $page->find('css', '.wds-tabs');
$linkEl = $subNav->find('css', 'li a');
var_dump($linkEl->getAttribute('href'));

echo "The link text is: ". $linkEl->getText() . "\n";

/*
Trouver via l'incroyable Named Selector

    En plus du CSS, il existe un autre moyen important de trouver des éléments à l'aide de Mink : il s'appelle le named sélecteur. Le named sélecteur consiste à trouver un élément par son texte visible.
    
        $selectorsHandler = $session->getSelectorsHandler();
        $linkEl = $page->find(
            'named',
            array(
                'link',
                $selectorsHandler->xpathLiteral('Books')
            )
        );
        echo "The link href is: ". $linkEl->getAttribute('href') . "\n";
        
        Au lieu de passer css à find(), cela passe named avec un tableau indiquant que nous recherchons un "lien" dont le texte est "Books". 
        Dans ce cas, nous utilisons le texte de la balise d'ancrage. Mais le Named Selector recherche également des correspondances sur l' title attribut, sur l' alt attribut d'une image à l'intérieur d'un lien et plusieurs autres choses. Il trouve des éléments en utilisant tout ce qu'un utilisateur ou un lecteur d'écran considère comme le "texte" d'un élément.

    Et au lieu d'utiliser ce gros bloc de code laid, vous utiliserez le named selector via $page->findLink(). Passez-le "Activité Wiki":
    
        $linkEl = $page->findLink('Books');
        echo "The link href is: ". $linkEl->getAttribute('href') . "\n";

    Le named sélecteur peut trouver 3 types d'éléments différents :

        des liens,z
        des champs (fields)
        et des boutons.

        Pour rechercher un champ, utilisez $page->findField().

        Cela fonctionne en trouvant le label qui correspond au mot "Description", puis trouve le champ associé à ce label.
        Pour trouver un bouton, utilisez $page->findButton(). Oh, et le named selector est "flou" - il ne correspondra donc qu'à une partie du texte d'un bouton, d'un champ ou d'un lien.

Cliquez déjà sur un lien

$linkEl->click();
echo "Page URL after click: ". $session->getCurrentUrl() . "\n";

Lorsque vous avez un seul élément, vous pouvez faire beaucoup de choses avec lui, et chacune est un simple appel de méthode. Nous avons focus, blur, dragTo, mouseOver, check, unCheck, doubleClicket à peu près tout ce que vous pouvez imaginer faire avec un élément.

GoutteDriver = cURL, Selenium2Driver = vrai navigateur

    Le pilote Goutte utilise cURL. Si nous voulions utiliser Selenium à la place, nous n'avons qu'à changer le pilote en $driver = new Selenium2Driver();:

Dans notre terminal, j'ai toujours le fichier Selenium JAR en arrière-plan.

    Se placer dans le dossier "drivers" où se trouve "selenium-server-4.1.2.jar"

    pour lancer le serveur selenium : terminal => java -jar selenium-server-4.1.2.jar standalone

terminal => php mink.php
*/

$driver = new Selenium2Driver('chrome');
$session = new Session($driver);
$session->start();
$session->visit('https://www.jeuxvideo.com/');
$page = $session->getPage();
//Accepter les cookies
$page->findButton('Accepter et accéder gratuitement')->click();
$linkEl = $page->findLink('Dune : après');
$linkEl->click();
//$session->stop();