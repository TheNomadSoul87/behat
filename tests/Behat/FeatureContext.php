<?php
declare(strict_types=1);

namespace App\Tests\Behat;

use App\DataFixtures\AppFixtures;
use Behat\Behat\Tester\Exception\PendingException;
use App\Entity\Product;
use App\Entity\User;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\Mink\Session;
use Behat\MinkExtension\Context\RawMinkContext;
use Doctrine\Bundle\FixturesBundle\Loader\SymfonyFixturesLoader;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\Assert;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use function PHPUnit\Framework\assertCount;
use function PHPUnit\Framework\assertNotNull;
use function PHPUnit\Framework\assertStringContainsString;

/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 * RawMinkContext permet d'accéder à la session, mais je peux l'enlever finalement car on importe la Session dans le constructor...
 */
final class FeatureContext extends RawMinkContext implements Context
{
    /** @var KernelInterface */
    private static $kernel;

    /** @var Response|null */
    private $response;

    /**
     * @var UserPasswordHasherInterface
     */
    private UserPasswordHasherInterface $encoder;

    /**
     * @var Session
     */
    private Session $session;

    /**
     * @var User
     */
    private User $currentUser;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     * @param KernelInterface $kernel
     * @param Session $session
     * @param UserPasswordHasherInterface $encoder
     */
    public function __construct(KernelInterface $kernel, Session $session, UserPasswordHasherInterface $encoder)
    {
        self::$kernel = $kernel;
        $this->encoder = $encoder;
        $this->session = $session;
    }

//    /**
//     * Returns Mink session.
//     *
//     * @param string|null $name name of the session OR active session will be used
//     *
//     * @return Session
//     * méthode  commentée car j'ai dans la documentation de FriendsOfBehat/SymfonyExtension, on montre une autre façon d'accéder à la session de mink (voir dans le constructeur)
//     */
//    public function getSession($name = null)
//    {
//        return $this->getMink()->getSession($name);
//    }

    /**
     * @return \Behat\Mink\Element\DocumentElement
     */
    private function getPage()
    {
        // return $this->getSession()->getPage();
        return $this->session->getPage();
    }

    /**
     * @return EntityManager|object|null
     */
    private function getEntityManager()
    {
        return self::$kernel->getContainer()->get('doctrine.orm.default_entity_manager');
    }

    /**
     * @When a demo scenario sends a request to :path
     */
    public function aDemoScenarioSendsARequestTo(string $path): void
    {
        $this->response = self::$kernel->handle(Request::create($path, 'GET'));
    }

    /**
     * @Then the response should be received
     */
    public function theResponseShouldBeReceived(): void
    {
        if ($this->response === null) {
            throw new \RuntimeException('No response received');
        }
        Assert::assertNotNull($this->response);
    }

    /**
     * @Given I am logged in as an admin
     */
    public function iAmLoggedInAsAnAdmin()
    {
        $this->currentUser = $this->thereIsAnAdminUserWithPassword('admin', 'admin');

        // Aller sur une page
        $this->visitPath('/login');
        $this->getPage()->fillField('Username', 'admin');
        $this->getPage()->fillField('Password', 'admin');
        $this->getPage()->pressButton('Sign in');
    }

    /**
     * @When I fill in the search box with :term
     * moi : créée par nous-même
     */
    public function iFillInTheSearchBoxWith($term)
    {
//        $searchBox = $this->getPage()->find('css', 'input[name="searchTerm"]');
//
//        assertNotNull($searchBox, 'Could not find the search box!');
//
//        $searchBox->setValue($term);

//        Nous avons également accès à un objet sympa appelé WebAssert via la méthode assertSession().
//        Remplacer getPage() par assertSession() et find() par elementExists() et supprimez l' assertNotNull().
//        La méthode elementExists() trouve l'élément et vérifie s'il existe à la fois :

        $searchBox = $this->assertSession()->elementExists('css', 'input[name="searchTerm"]');

        $searchBox->setValue($term);
    }

    /**
     * @When I press the search button
     * moi : créée par nous-même
     */
    public function iPressTheSearchButton()
    {
        $button = $this->getPage()
            ->find('css', '#search_submit');

        assertNotNull($button, 'Could not find the search button!');

        $button->press();
    }

    /**
     * @Given there is an admin user :username with password :password
     */
    public function thereIsAnAdminUserWithPassword($username, $password)
    {
        $user = new User();
        $user->setUsername($username);
        $user->setPassword($this->encoder->hashPassword($user, $password));
        $user->setRoles(array('ROLE_ADMIN'));

        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();

        return $user;
    }

    /**
     * @Given there is/are :count product(s)
     */
    public function thereAreProducts($count)
    {
        $this->createProducts($count);
    }

    /**
     * @Given I author :count products
     */
    public function iAuthorProducts($count)
    {
        $this->createProducts($count, $this->currentUser);
    }

    /**
     * @Given the following products exist:
     */
    public function theFollowingProductsExist(TableNode $table)
    {
        /* foreach ($table as $row) {
            print_r($row);
        }*/

        foreach ($table as $row) {
            $product = new Product();
            $product->setName($row['name']);
            $product->setPrice(rand(10, 1000));
            $product->setDescription('lorem');
            if (isset($row['is published']) && $row['is published'] == 'yes') {
                $product->setIsPublished(true);
            }
            $this->getEntityManager()->persist($product);
        }
        $this->getEntityManager()->flush();
    }

    /**
     * @When I click :linkText
     */
    public function iClick($linkName)
    {
        // On commente le code suivant car il existe la méthode clickLink qui fait pareil
        // $link= $this->getPage()->findLink($linkName); (rem : on peut remplacer "$this->getPage()" par un élément html plus précis => ex : voir "cliquer sur un bouton")
        //
        // assertNotNull($link, 'Cannot find link in row with text '.$linkName);

        // Cliquer un lien
        $this->getPage()->clickLink($linkName);
    }

    /**
     * @Then I should see :count products
     */
    public function iShouldSeeProducts($count)
    {
        $table = $this->getPage()->find('css', 'table.table');
        assertNotNull($table, 'Cannot find a table!');
        assertCount(intval($count), $table->findAll('css', 'tbody tr'));
    }

    /**
     * @When I wait for the modal to load
     */
    public function iWaitForTheModalToLoad()
    {
    // Le deuxième argument de wait() est une expression JavaScript qui s'exécutera sur votre page toutes les 100 millisecondes
    // Dès que cela devient vrai, Mink arrête d'attendre et passe à l'étape suivante. Lorsque la fenêtre modal de Bootstrap s'ouvre, un élément avec la classe modal devient visible.
    // Cela dit maintenant : "Attendez que cette expression JavaScript soit vraie ou attendez un maximum de 5 secondes."
        $this->session->wait(5000, "$('.modal:visible').length > 0");
    }

    /**
     * @BeforeSuite
     * Nous voulons insérer un nouvel utilisateur dans la base de données avec ce nom d'utilisateur et ce mot de passe.
     * Si Symfony était chargé, ce serait vraiment facile
     * Nous n'avons besoin de démarrer Symfony qu'une seule fois au début de la suite de tests.
     */
    public static function bootstrapSymfony()
    {
        self::$kernel->boot();
    }

    /**
     * @BeforeScenario
     * Moi : ça purge ma base de données de test
     */
    public function clearData()
    {
        $em = $this->getEntityManager();

        // $em->createQuery('DELETE App\Entity\Product p')->execute();
        // $em->createQuery('DELETE App\Entity\User u')->execute();

        // Ca serait pénible de devoir enchaîner les requêtes pour nettoyer la base de données
        // On peut faire plus rapide en faisant :
        $purger = new ORMPurger($em);
        $purger->purge();
        // Rem : attention, si tu effaces la table Product, les téléphones seront effacés de la bdd et le test de recherche (Then I should see "<result>" avec result = Samsung Galaxy S II) ne fonctionnera plus
    }

    /**
     * @BeforeScenario @fixtures
     * Cette méthode doit être placé après la méthode clearData() car à cause du true plus bas, on viole une contrainte d'unicité des entités
     *
     * terminal => ./vendor/bin/behat --tags=fixtures
     *      n'exécutera que des scénarios étiquetés avec des @fixtures
     *
     * terminal => ./vendor/bin/behat --tags=~fixtures
     *      exécutera tous les scénarios sauf ceux étiquetés avec des @fixtures
     *
     * terminal => ./vendor/bin/behat --tags=~fixtures -v
     *      option de verbosité qui vous montrera la trace complète de la pile si quelque chose ne va pas !
     */
    public function loadFixtures()
    {
        // $loader = new ContainerAwareLoader(self::$kernel->getContainer());
        // $loader->loadFromDirectory(__DIR__.'/../../src/DataFixtures');

        // Un purgeur peut être passé en deuxième argument si vous souhaitez effacer des données. Nous le faisons déjà (voir méthode clearData), donc je ne vais pas m'en soucier ici.
        // $executor = new ORMExecutor($this->getEntityManager());

        // le true dit de ne pas supprimer les données, mais de les ajouter à la place.
        // $executor->execute($test->getFixtures(), true);

        (new AppFixtures($this->getEntityManager(), $this->encoder))->load($this->getEntityManager());
    }

    private function createProducts($count, User $author = null)
    {
        for ($i = 0; $i < $count; $i++) {
            $product = new Product();
            $product->setName('Product '.$i);
            $product->setPrice(rand(10, 1000));
            $product->setDescription('lorem');

            if ($author) {
                $product->setAuthor($author);
            }

            $this->getEntityManager()->persist($product);
        }

        $this->getEntityManager()->flush();
    }

    /**
     * Pauses the scenario until the user presses a key. Useful when debugging a scenario.
     *
     * @Then (I )break
     * Autre manière de debugger => poser un breakpoint (utile pour du javascript comme l'ouverture d'une fenêtre modale)
     * Laissez-moi vous montrer l'outil de débogage : Behatch/contexts (https://github.com/Behatch/contexts). Il s'agit d'une bibliothèque open source qui contient un tas de contextes utiles - des classes comme FeatureContextet MinkContext avec des définitions libres. Par exemple, cela a un BrowserContext que vous pouvez apporter dans votre projet pour obtenir un tas de définitions utiles.
     * Je n'utilise pas cette bibliothèque directement, mais je la vole. La DebugContext classe a une de mes définitions préférées : iPutABreakPoint(). Copiez-le et déposez-la ici.
     * Voir "And break" dans features/web/product_admin.feature
     * Si une étape échoue, le navigateur se fige et dans le terminal de phpstorm on nous demande d'appuyer sur Entrée pour continuer. Ca nous donne le temps de voir ce qui ne va pas
     */
    public function iPutABreakpoint()
    {
        fwrite(STDOUT, "\033[s    \033[93m[Breakpoint] Press \033[1;93m[RETURN]\033[0;93m to continue...\033[0m");

        while (fgets(STDIN, 1024) == '') {}

        fwrite(STDOUT, "\033[u");

        return;
    }

    /**
     * Saving a screenshot
     *
     * @When I save a screenshot to :filename
     * Prendre des captures d'écran (https://symfonycasts.com/screencast/behat/javascript-debugging-screenshots)
     * Dans https://github.com/Behatch/contexts), copier la méthode iSaveAScreenshotIn
     * Voir "And I save a screenshot to "shot.png"" dans features/web/product_admin.feature
     *
     * Enregistrement des captures d'écran en cas d'échec :
     * En utilisant le crochet @AfterScenario, vous pouvez enregistrer automatiquement une capture d'écran à chaque échec.
     * Consultez notre article de blog à ce sujet : Behat on CircleCI with Failure Screenshots .
     */
    public function iSaveAScreenshotIn($filename)
    {
        sleep(1);
        $this->saveScreenshot($filename, __DIR__.'/../..');
    }

    /**
     * @Then the :rowText row should have a check mark
     */
    public function theRowShouldHaveACheckMark($rowText)
    {
        $row = $this->findRowByText($rowText);

        assertStringContainsString('fa-check', $row->getHtml(), 'Could not find the fa-check element in the row!');
    }

    /**
     * @When I press :linkText in the :rowText row
     */
    public function iClickInTheRow($linkText, $rowText)
    {
        $row = $this->findRowByText($rowText);

        // On commente le code suivant car il existe la méthode pressButton qui fait pareil
        // $button = $row->findButton($linkText);
        //
        // assertNotNull($button, 'Cannot find link in row with text '.$linkText);

        // cliquer sur un bouton
        $row->pressButton($linkText);
    }

    /**
     * @param $rowText
     * @return \Behat\Mink\Element\NodeElement
     */
    private function findRowByText($rowText)
    {
        $row = $this->getPage()->find('css', sprintf('table tr:contains("%s")', $rowText));

        assertNotNull($row, 'Cannot find a table row with this text!');

        return $row;
    }
}