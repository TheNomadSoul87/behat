.PHONY: help
.DEFAULT_GOAL = help

SYMFONY=@symfony
SYMFONY_CONSOLE=$(SYMFONY) console
DOCKER=@docker
DOCKER-COMPOSE=@docker-compose
COMPOSER=@composer

install: ## installation des dépendances
	$(COMPOSER) install

update: ## mises à jour des dépendances et du fichier composer.lock
	$(COMPOSER) update

clean-db: ## Réinitialiser la base de donnée avec migrations
	$(SYMFONY_CONSOLE) d:d:d --force
	$(SYMFONY_CONSOLE) d:d:c
	$(SYMFONY_CONSOLE) d:m:m --no-interaction

force-schema-database: ## Réinitialiser la base de donnée avec schéma
	$(SYMFONY_CONSOLE) d:d:d --force
	$(SYMFONY_CONSOLE) d:d:c
	$(SYMFONY_CONSOLE) doctrine:schema:create

migration: ## Créer une nouvelle migration
	$(SYMFONY_CONSOLE) m:mi

migrate: migration ## Exécuter une nouvelle migration
	$(SYMFONY_CONSOLE) d:m:m --no-interaction

start: stop## Démarrer le serveur web Symfony
	$(SYMFONY) serve -d

stop: ## Arrêter le serveur web Symfony
	$(SYMFONY) server:stop

docker: ## Création et lancement des conteneurs
	$(DOCKER-COMPOSE) down
	$(DOCKER-COMPOSE) up -d

docker-sup: ## Supprimer tous les conteneurs actifs ou non d'un coup
	$(DOCKER) rm -f $$(docker ps -aq)

docker-database: ## ajouter un conteneur mysql(ou autre bdd) à notre fichier docker-compose.yaml
	$(SYMFONY_CONSOLE) make:docker:database

container: ## Affiche les services dans le conteneur
	$(SYMFONY_CONSOLE) debug:container

autowiring: ## Liste des classes/interfaces qu'on peut "autowiring"
	$(SYMFONY_CONSOLE) debug:autowiring

help: ## Liste des commandes
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'