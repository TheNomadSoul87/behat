<?php

namespace App\Controller;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductAdminController extends AbstractController
{
    /**
     * @var ManagerRegistry
     */
    private ManagerRegistry $doctrine;

    /**
     * ProductAdminController constructor.
     */
    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    #[Route('/admin/products', name: 'product_list')]
    public function list(ProductRepository $productRepository)
    {
        $products = $productRepository->findAll();

        return $this->render('product/list.html.twig', [
            'products' => $products
        ]);
    }

    #[Route('/admin/products/new', name: 'product_new')]
    public function new(Request $request)
    {
        if ($request->isMethod('POST')) {
            $this->addFlash('success', 'Product created FTW!');

            $product = new Product();
            $product->setName($request->get('name'));
            $product->setDescription($request->get('description'));
            $product->setPrice($request->get('price'));
            $product->setAuthor($this->getUser());

            $em = $this->doctrine->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('product_list');
        }
        return $this->render('product/new.html.twig');
    }

    #[Route('/admin/products/delete/{id}', name: 'product_delete', methods: ["POST"])]
    public function delete(Product $product)
    {
        $em = $this->doctrine->getManager();
        $em->remove($product);
        $em->flush();
        $this->addFlash('success', 'The product was deleted');
        return $this->redirectToRoute('product_list');
    }
}
