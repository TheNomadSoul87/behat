<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @var ProductRepository
     */
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    #[Route('/', name: 'homepage')]
    public function homepage()
    {
        $products = $this->productRepository->search('');

        return $this->render('main/homepage.html.twig', [
            'products' => $products
        ]);
    }

    #[Route('/search', name: 'product_search')]
    public function search(Request $request)
    {
        $search = $request->query->get('searchTerm');

        $products = $this->productRepository->search($search);

        return $this->render('main/homepage.html.twig', [
            'products' => $products,
            'search' => $search
        ]);
    }

    #[Route('/admin', name: 'admin')]
    public function admin()
    {
        return $this->render('main/admin.html.twig');
    }

//    #[Route('/_db/rebuild', name: 'db_rebuild')]
//    public function dbRebuild()
//    {
//        $schemaManager = $this->get('schema_manager');
//        $schemaManager->rebuildSchema();
//        $schemaManager->loadFixtures();
//
//        return new JsonResponse(array(
//            'success' => true
//        ));
//    }
}
