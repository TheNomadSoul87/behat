Feature: ls
  In order to see the directory structure
  As a UNIX user
  I need to be able to list the current directory's contents

  Background:
    Given I have a file named "john"

#  Le Background s'exécute en fait avant le scénario.
#
#  Deuxièmement, lorsque vous avez des doublons qui ne figurent pas sur la première ligne de tous vos scénarios, comme le "Alors je devrais voir....", vous pouvez utiliser des contours de scénario. C'est un peu moins utilisé, mais nous y reviendrons un peu plus tard.

  Scenario: List 2 files in a directory
#    Given I have a file named "john"
#    And I have a file named "hammond"
    Given I have a file named "hammond"
    When I run "ls"
    Then I should see "john" in the output
    And I should see "hammond" in the output

  Scenario: List 1 file and 1 directory
#    Given I have a file named "john"
#    And I have a dir named "ingen"
    Given I have a dir named "ingen"
    When I run "ls"
    Then I should see "john" in the output
    And I should see "ingen" in the output