Feature: Product admin panel
  In order to maintain the products shown on the site
  As an admin
  I need to be able to add/edit/delete products

  Background:
    Given I am logged in as an admin

  Scenario: List available products
#    Given I am logged in as an admin
    Given there are 5 products
    And there is 1 product
    And I am on "/admin"
    When I click "Products"
    Then I should see 6 products

  # Pour lancer un seul scénario : ./vendor/bin/behat features/web/product_admin.feature:14
  Scenario: Products show owner
#    Given I am logged in as an admin
    Given I author 5 products
    When I go to "/admin/products"
    # no products will be anonymous
    Then I should not see "Anonymous"

  Scenario: Show published/unpublished
    Given the following products exist:
      | name | is published |
      | Foo1 | yes          |
      | Foo2 | no           |
    When I go to "/admin/products"
    Then the "Foo1" row should have a check mark

  Scenario: Deleting a product
    Given the following products exist:
      | name |
      | Bar  |
      | Foo1 |
    When I go to "/admin/products"
    And I press "Delete" in the "Foo1" row
    Then I should see "The product was deleted"
    And I should not see "Foo1"
    But I should see "Bar"

  # On a ajouté @javascript pour les besoins de la partie "Maitrisez Javascript..."
  # Assurez-vous que le serveur Selenium fonctionne
  # remarque aabbaa :
  #     Quand je mets @javascript => il faut relancer le server Symfony en activant l'environnement test sinon c'est notre base de données de développement qui sera affectée et non la base de données de test lors des actions réalisée automatiquement dans le navigateur web !!!
  #         => APP_ENV=test symfony serve -d
  @javascript
  Scenario: Add a new product
#    Given I am logged in as an admin
    Given I am on "/admin/products"
    When I click "New Product"
    # On a ajouté "And I wait for the modal to load" pour les besoins de la partie "Maitrisez Javascript..."
    # quand on clique sur un lien le submit d'un formulaire et que cela provoque une actualisation complète de la page, Mink et Selenium attendront cette actualisation de la page. Mais, si vous faites quelque chose en JavaScript, Selenium n'attend pas.
    # Dans notre cas on clique sur le lien "Nouveaux produits" et on recherche immédiatement le champ "Nom". Si ce champ n'est pas là presque instantanément, il échoue. Donc nous devons faire attendre Selenium après avoir cliqué sur le lien.
    And I wait for the modal to load
#    And break
    And I save a screenshot to "shot.png"
    And I fill in "Name" with "Veloci-chew toy"
    And I fill in "Price" with "20"
    And I fill in "Description" with "Have your velociraptor chew on this instead!"
    And I press "Save"
    Then I should see "Product created FTW!"
    And I should see "Veloci-chew toy"