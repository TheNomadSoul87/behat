Feature: Search
  In order to find products dinosaurs love
  As a website user
  I need to be able to search for products

  Background:
    Given I am on "/"

  # @javascript -> décommenter pour vraiment ouvrir le navigateur
#  Scenario: Search for a word that exists
#    When I fill in "searchTerm" with "Samsung"
#    And I press "search_submit"
#    Then I should see "Samsung Galaxy S II"
#
#  Scenario: Search for a word that does not exist
#    When I fill in "searchTerm" with "XBox"
#    And I press "search_submit"
#    Then I should see "No products found"

#  Scenario Outline: Search for a product
#    When I fill in "searchTerm" with "<term>"
#    And I press "search_submit"
#    Then I should see "<result>"
#    Examples:
#      | term    | result              |
#      | Samsung | Samsung Galaxy S II |
#      | XBox    | No products found   |

# le code de ce fichier n'utilise pas les selecteurs css mais le named selector.
#   ex : searchTerm est l'attribut name du champ de recherche.
#   ex : search_submit est l'id du bouton de soumission.
#   Tu t'en rends compte que tu fais un ctrl+clic sur "And I press "search_submit" et que tu regardes les fonctions intégrées par Behat qui sont appelées
#   Presque toutes les définitions intégrées trouvent des éléments en utilisant le named selector, pas CSS.

#  Mais s'il vous plaît s'il vous plaît - n'utilisez pas l'attribut name ou l'id. En fait, la règle cardinale de Behat est que vous ne devez jamais utiliser de sélecteurs CSS ou d'autres éléments techniques dans votre scénario. Pourquoi? Parce que la personne qui bénéficie de la fonctionnalité est un internaute, et nous écrivons ceci de son point de vue. Un internaute ne comprend pas ce qu'il veut searchTermdire search_submit. Cela rend votre scénario moins utile : c'est du jargon technique au lieu de descriptions de comportement.
#
#  Alors pourquoi avons-nous triché ? Eh bien, le champ de recherche n'a pas d'étiquette et le bouton n'a pas de texte. Je ne peux pas utiliser le sélecteur nommé pour les trouver, sauf si je triche. Sinon on peut créer une step definition personnalisée


#  Création de step definition personnalisée pour "I fill in the search box with "<term>" et "I press the search button"
#  https://symfonycasts.com/screencast/behat/mink-inside-feature-context
#  Pour générer les méthodes manquantes : terminal =>  ./vendor/bin/behat features/search.feature
#  Vous pouvez ajouter un --append-snippets à la commande précédente et Behat mettra les méthodes à l'intérieur de la FeatureContext classe pour vous

  # Le scénario échoue car notre base de données de test ne contient aucune donnée.
  # Je pense que la saisie manuelle des données avec un Given est la manière la plus lisible de faire les choses. Mais, si vous chargez les fixtures, voici la meilleure façon.Scenario:
  # Au lieu de cela, j'ai besoin d'un moyen de baliser les scénarios pour dire "celui-ci a besoin de fixtures".
  # Ajoutez @fixtures

  @fixtures
  Scenario Outline: Search for a product
    When I fill in the search box with "<term>"
    And I press the search button
    Then I should see "<result>"
    Examples:
      | term    | result              |
      | Samsung | Samsung Galaxy S II |
      | XBox    | No products found   |