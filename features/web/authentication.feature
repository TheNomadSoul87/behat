Feature: Authentication
  In order to gain access to the site management area
  As an admin
  I need to be able to login and logout

  Scenario: Logging in
    Given there is an admin user "admin" with password "admin"
    And I am on "/"
#    je clique sur "Login" (cliquer sur un lien)
    When I follow "Login"
    And I fill in "Username" with "admin"
    # On fait exprès de mettre un mauvais mot de passe pour expliquer comment debugger
#    And I fill in "Password" with "adminpass"
    And I fill in "Password" with "admin"
#    je clique sur "Sign in" (cliquer sur un bouton)
    And I press "Sign in"
#    La ligne suivant est utile pour debugger. Il faut la placer juste avant l'étape défaillante afin qu'il affiche le html de la page dans le terminal
#    Then print last response
#    La ligne suivant permet de montrer la page de l'étape défaillante dans un navigateur (voir show_cmd: 'google-chrome %s' dans le fichier behat.yml)
#    Then show last response
    Then I should see "Logout"